module CristoShoppingList {
    requires javafx.fxml;
    requires javafx.controls;
    requires javafx.web;

    requires java.sql;

    requires com.google.common;
    requires evo.inflector;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.ikonli.fontawesome;
    requires com.jfoenix;
    requires org.apache.logging.log4j;
    requires org.apache.logging.log4j.core;
    requires org.jooq;
    requires com.google.zxing; //zxing
    requires okapibarcode;

    requires org.controlsfx.controls;

    requires java.desktop;

    opens shoppinglist;
    opens shoppinglist.controller to javafx.fxml;
    opens shoppinglist.ui.content to javafx.fxml;
    opens shoppinglist.ui.content.settings to javafx.fxml;
    opens shoppinglist.ui.layouts to javafx.fxml;
    opens shoppinglist.model to javafx.base, org.jooq;
}