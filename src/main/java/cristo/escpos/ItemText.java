package cristo.escpos;

import cristo.util.Str;

import java.util.ArrayList;
import java.util.List;

public class ItemText {
    private String leftText;
    private String rightText;

    private List<String> leftTextLines = new ArrayList<>();

    private int maxLeftText = 22;
    private int maxRightText = 7;
    private int minSpace = 3;

    private int tabPadding = 25;

    public ItemText(String leftText, String rightText) {
        setLeftText(leftText);
        setRightText(rightText);
        buildleftTextLines();
    }

    public String getLeftRow(int index) {
        if (index >= leftTextLines.size()) {
            return null;
        }
        return leftTextLines.get(index);
    }

    public List<String> getLeftTextLines() {
        buildleftTextLines();
        return leftTextLines;
    }

    private void buildleftTextLines() {
        this.leftTextLines = Str.justifyToArray(leftText, maxLeftText);
    }

    private void setBFont() {
        setMaxLeftText(30);
    }

    public String getLeftText() {
        return leftText;
    }

    public void setLeftText(String leftText) {
        this.leftText = leftText;
    }

    public String getRightText() {
        return rightText;
    }

    public void setRightText(String rightText) {
        this.rightText = rightText;
    }

    public int getMaxLeftText() {
        return maxLeftText;
    }

    public void setMaxLeftText(int maxLeftText) {
        this.maxLeftText = maxLeftText;
    }

    public int getMaxRightText() {
        return maxRightText;
    }

    public void setMaxRightText(int maxRightText) {
        this.maxRightText = maxRightText;
    }

    public int getMinSpace() {
        return minSpace;
    }

    public void setMinSpace(int minSpace) {
        this.minSpace = minSpace;
    }
}
