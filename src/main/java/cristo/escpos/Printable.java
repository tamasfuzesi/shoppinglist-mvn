package cristo.escpos;

public interface Printable {
    void print();
}
