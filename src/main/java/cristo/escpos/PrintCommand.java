package cristo.escpos;

import cristo.util.Config;
import cristo.util.Printer;

import javax.print.*;

public class PrintCommand {
    private byte[] bytes;

    public PrintCommand(byte[] bytes) {
        this.bytes = bytes;
    }

    public void print() {
        try {
            Printer printer = new Printer();
            PrintService selectedPrinterService = printer.findService((String) Config.get("selected_printer"));
            PrintService printService = selectedPrinterService != null
                    ? selectedPrinterService
                    : printer.getDefaultService();
            Doc doc = new SimpleDoc(bytes, DocFlavor.BYTE_ARRAY.AUTOSENSE, null);
            DocPrintJob printJob = printService.createPrintJob();
            printJob.print(doc, null);
        } catch (PrintException e) {
            e.printStackTrace();
        }
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }
}
