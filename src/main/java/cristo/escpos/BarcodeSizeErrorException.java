package cristo.escpos;

public class BarcodeSizeErrorException extends Exception {

    public BarcodeSizeErrorException() {
        super();
    }

    public BarcodeSizeErrorException(String message) {
        super(message);
    }
}