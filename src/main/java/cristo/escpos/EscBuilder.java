package cristo.escpos;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;


public class EscBuilder {
    private ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    private String charCode = "CP852";

    public EscBuilder(String charCode) {
        this();
        init();
        setCharCode("hun");
    }

    public EscBuilder() {

    }

    public byte[] getBytes() {
        return this.byteArrayOutputStream.toByteArray();
    }

    public void raw(byte[] bytes) {
        try {
            byteArrayOutputStream.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void print(String text) throws ConnectionException {
        try {
            byteArrayOutputStream.write(text.getBytes(this.getCharCode()));
        } catch (UnsupportedEncodingException e) {
            throw new ConnectionException("Unable to print text", e);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void tab() throws ConnectionException {
        try {
            byteArrayOutputStream.write(Command.HT);
        } catch (UnsupportedEncodingException e) {
            throw new ConnectionException("Unable to print text", e);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setTabPosition(int position) {
        try {
            byte[] esc_d = Command.ESC_D;
            esc_d[2] = (byte) position;
            byteArrayOutputStream.write(esc_d);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printLn(String text) throws ConnectionException, IOException {
        print(text + "\n");
    }

    public void lineBreak() throws IOException {
        lineBreak(1);
    }

    public void lineBreak(int nbLine) throws IOException {
        for (int i = 0; i < nbLine; i++) {
            byteArrayOutputStream.write(Command.CTL_LF);
        }
    }

    public void printQRCode(String value, int size) {
        try {
            QRCodeGenerator q = new QRCodeGenerator();
            printImage(q.generate(value, size));
        } catch (QRCodeException | IOException e) {
            e.printStackTrace();
        }
    }

    public void printQRCode(String value) {
        printQRCode(value, 150);
    }

    public void printImage(String filePath) throws IOException {
        File img = new File(filePath);
        printImage(ImageIO.read(img));
    }

    public void printImage(BufferedImage image) throws IOException {
        Image img = new Image();
        int[][] pixels = img.getPixelsSlow(image);
        for (int y = 0; y < pixels.length; y += 24) {
            byteArrayOutputStream.write(Command.LINE_SPACE_24);
            byteArrayOutputStream.write(Command.SELECT_BIT_IMAGE_MODE);
            byteArrayOutputStream.write(new byte[]{(byte) (0x00ff & pixels[y].length), (byte) ((0xff00 & pixels[y].length) >> 8)});
            for (int x = 0; x < pixels[y].length; x++) {
                byteArrayOutputStream.write(img.recollectSlice(y, x, pixels));
            }

            byteArrayOutputStream.write(Command.CTL_LF);
        }
        byteArrayOutputStream.write(Command.CTL_LF);
        byteArrayOutputStream.write(Command.LINE_SPACE_30);
    }

    public void setTextSizeNormal() {
        setTextSize(1, 1);
    }

    public void setTextSize2H() {
        setTextSize(1, 2);
    }

    public void setTextSize2W() {
        setTextSize(2, 1);
    }

    public void setText4Square() {
        setTextSize(2, 2);
    }

    private void setTextSize(int width, int height) {
        try {
            if (height == 2 && width == 2) {
                byteArrayOutputStream.write(Command.TXT_NORMAL);
                byteArrayOutputStream.write(Command.TXT_4SQUARE);
            } else if (height == 2) {
                byteArrayOutputStream.write(Command.TXT_NORMAL);
                byteArrayOutputStream.write(Command.TXT_2HEIGHT);
            } else if (width == 2) {
                byteArrayOutputStream.write(Command.TXT_NORMAL);
                byteArrayOutputStream.write(Command.TXT_2WIDTH);
            } else {
                byteArrayOutputStream.write(Command.TXT_NORMAL);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setTextTypeBold() {
        setTextType("B");
    }

    public void setTextTypeUnderline() {
        setTextType("U");
    }

    public void setTextType2Underline() {
        setTextType("U2");
    }

    public void setTextTypeBold2Underline() {
        setTextType("BU2");
    }

    public void setTextTypeNormal() {
        setTextType("NORMAL");
    }

    private void setTextType(String type) {
        try {
            if (type.equalsIgnoreCase("B")) {
                byteArrayOutputStream.write(Command.TXT_BOLD_ON);
                byteArrayOutputStream.write(Command.TXT_UNDERL_OFF);
            } else if (type.equalsIgnoreCase("U")) {
                byteArrayOutputStream.write(Command.TXT_BOLD_OFF);
                byteArrayOutputStream.write(Command.TXT_UNDERL_ON);
            } else if (type.equalsIgnoreCase("U2")) {
                byteArrayOutputStream.write(Command.TXT_BOLD_OFF);
                byteArrayOutputStream.write(Command.TXT_UNDERL2_ON);
            } else if (type.equalsIgnoreCase("BU")) {
                byteArrayOutputStream.write(Command.TXT_BOLD_ON);
                byteArrayOutputStream.write(Command.TXT_UNDERL_ON);
            } else if (type.equalsIgnoreCase("BU2")) {
                byteArrayOutputStream.write(Command.TXT_BOLD_ON);
                byteArrayOutputStream.write(Command.TXT_UNDERL2_ON);
            } else if (type.equalsIgnoreCase("NORMAL")) {
                byteArrayOutputStream.write(Command.TXT_BOLD_OFF);
                byteArrayOutputStream.write(Command.TXT_UNDERL_OFF);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void cutPart() {
        cut("PART");
    }

    public void cutFull() {
        cut("FULL");
    }

    private void cut(String mode) {
        try {
            for (int i = 0; i < 6; i++) {
                byteArrayOutputStream.write(Command.CTL_LF);
            }
            if (mode.toUpperCase().equals("PART")) {
                byteArrayOutputStream.write(Command.PAPER_PART_CUT);
            } else {
                byteArrayOutputStream.write(Command.PAPER_FULL_CUT);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printBarcode(String code, String bc, int width, int height, String pos, String font) {
        try {
            // Align Bar Code()
//            byteArrayOutputStream.write(Command.TXT_ALIGN_CT);
            //Width
            if (width >= 1 || width <= 255) {
                byteArrayOutputStream.write(Command.BARCODE_WIDTH);
            } else {
                throw new BarcodeSizeErrorException("Incorrect Width");
            }
            // Height
            if (height >= 2 || height <= 6) {
                byteArrayOutputStream.write(Command.BARCODE_HEIGHT);
            } else {
                throw new BarcodeSizeErrorException("Incorrect Height");
            }
            //Font
            if (font.equalsIgnoreCase("B")) {
                byteArrayOutputStream.write(Command.BARCODE_FONT_B);
            } else {
                byteArrayOutputStream.write(Command.BARCODE_FONT_A);
            }
            //Position
            if (pos.equalsIgnoreCase("OFF")) {
                byteArrayOutputStream.write(Command.BARCODE_TXT_OFF);
            } else if (pos.equalsIgnoreCase("BOTH")) {
                byteArrayOutputStream.write(Command.BARCODE_TXT_BTH);
            } else if (pos.equalsIgnoreCase("ABOVE")) {
                byteArrayOutputStream.write(Command.BARCODE_TXT_ABV);
            } else {
                byteArrayOutputStream.write(Command.BARCODE_TXT_BLW);
            }
            //Type
            switch (bc.toUpperCase()) {
                case "UPC-A":
                    byteArrayOutputStream.write(Command.BARCODE_UPC_A);
                    break;
                case "UPC-E":
                    byteArrayOutputStream.write(Command.BARCODE_UPC_E);
                    break;
                case "EAN13":
                    byteArrayOutputStream.write(Command.BARCODE_EAN13);
                    break;
                case "EAN8":
                    byteArrayOutputStream.write(Command.BARCODE_EAN8);
                    break;
                case "CODE39":
                    byteArrayOutputStream.write(Command.BARCODE_CODE39);
                    break;
                case "ITF":
                    byteArrayOutputStream.write(Command.BARCODE_ITF);
                    break;
                case "NW7":
                    byteArrayOutputStream.write(Command.BARCODE_NW7);
                    break;
            }
            //Print Code
            if (!code.equals("")) {
                byte[] tempCodeData = code.getBytes();
                byteArrayOutputStream.write(tempCodeData.length);
                byteArrayOutputStream.write(tempCodeData);
                byteArrayOutputStream.write(Command.CTL_LF);
            } else {
                throw new BarcodeSizeErrorException("Incorrect Value");
            }
        } catch (BarcodeSizeErrorException | IOException e) {
            e.printStackTrace();
        }
    }

    public void setTextFontA() {
        setTextFont("A");
    }

    public void setTextFontB() {
        setTextFont("B");
    }

    private void setTextFont(String font) {
        try {
            if (font.equalsIgnoreCase("B")) {
                byteArrayOutputStream.write(Command.TXT_FONT_B);
            } else {
                byteArrayOutputStream.write(Command.TXT_FONT_A);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setTextAlignCenter() {
        setTextAlign("CENTER");
    }

    public void setTextAlignRight() {
        setTextAlign("RIGHT");
    }

    public void setTextAlignLeft() {
        setTextAlign("LEFT");
    }

    private void setTextAlign(String align) {
        try {
            if (align.equalsIgnoreCase("CENTER")) {
                byteArrayOutputStream.write(Command.TXT_ALIGN_CT);
            } else if (align.equalsIgnoreCase("RIGHT")) {
                byteArrayOutputStream.write(Command.TXT_ALIGN_RT);
            } else {
                byteArrayOutputStream.write(Command.TXT_ALIGN_LT);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setTextDensity(int density) {
        try {
            switch (density) {
                case 0:
                    byteArrayOutputStream.write(Command.PD_N50);
                    break;
                case 1:
                    byteArrayOutputStream.write(Command.PD_N37);
                    break;
                case 2:
                    byteArrayOutputStream.write(Command.PD_N25);
                    break;
                case 3:
                    byteArrayOutputStream.write(Command.PD_N12);
                    break;
                case 4:
                    byteArrayOutputStream.write(Command.PD_0);
                    break;
                case 5:
                    byteArrayOutputStream.write(Command.PD_P12);
                    break;
                case 6:
                    byteArrayOutputStream.write(Command.PD_P25);
                    break;
                case 7:
                    byteArrayOutputStream.write(Command.PD_P37);
                    break;
                case 8:
                    byteArrayOutputStream.write(Command.PD_P50);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setTextNormal() {
        setTextProperties("LEFT", "A", "NORMAL", 1, 1, 9);
    }

    public void setTextProperties(String align, String font, String type, int width, int height, int density) {
        setTextAlign(align);
        setTextFont(font);
        setTextType(type);
        setTextSize(width, height);
        setTextDensity(density);
    }

    public String getCharCode() {
        return charCode;
    }

    public void setCharCode(String code) {
        try {
            switch (code.toUpperCase()) {
                case "USA":
                    byteArrayOutputStream.write(Command.CHARCODE_PC437);
                    break;
                case "JIS":
                    byteArrayOutputStream.write(Command.CHARCODE_JIS);
                    break;
                case "MULTILINGUAL":
                    byteArrayOutputStream.write(Command.CHARCODE_PC850);
                    break;
                case "PORTUGUESE":
                    byteArrayOutputStream.write(Command.CHARCODE_PC860);
                    break;
                case "CA_FRENCH":
                    byteArrayOutputStream.write(Command.CHARCODE_PC863);
                    break;
                default:
                case "NORDIC":
                    byteArrayOutputStream.write(Command.CHARCODE_PC865);
                    break;
                case "WEST_EUROPE":
                    byteArrayOutputStream.write(Command.CHARCODE_WEU);
                    break;
                case "GREEK":
                    byteArrayOutputStream.write(Command.CHARCODE_GREEK);
                    break;
                case "HEBREW":
                    byteArrayOutputStream.write(Command.CHARCODE_HEBREW);
                    break;
                case "WPC1252":
                    byteArrayOutputStream.write(Command.CHARCODE_PC1252);
                    break;
                case "CIRILLIC2":
                    byteArrayOutputStream.write(Command.CHARCODE_PC866);
                    break;
                case "LATIN2":
                case "CP852":
                    this.charCode = "cp852";
                    byteArrayOutputStream.write(Command.CHARCODE_PC852);
                    break;
                case "HUN":
                    this.charCode = "cp852";
                    byteArrayOutputStream.write(Command.CODEPAGE_HUNGARIAN);
                    break;
                case "EURO":
                    byteArrayOutputStream.write(Command.CHARCODE_PC858);
                    break;
                case "THAI42":
                    byteArrayOutputStream.write(Command.CHARCODE_THAI42);
                    break;
                case "THAI11":
                    byteArrayOutputStream.write(Command.CHARCODE_THAI11);
                    break;
                case "THAI13":
                    byteArrayOutputStream.write(Command.CHARCODE_THAI13);
                    break;
                case "THAI14":
                    byteArrayOutputStream.write(Command.CHARCODE_THAI14);
                    break;
                case "THAI16":
                    byteArrayOutputStream.write(Command.CHARCODE_THAI16);
                    break;
                case "THAI17":
                    byteArrayOutputStream.write(Command.CHARCODE_THAI17);
                    break;
                case "THAI18":
                    byteArrayOutputStream.write(Command.CHARCODE_THAI18);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void selftest() throws IOException {
        byteArrayOutputStream.write(Command.SELFTEST);
    }

    public void init() {
        try {
            byteArrayOutputStream.write(Command.HW_INIT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void openCashDrawerPin2() throws IOException {
        byteArrayOutputStream.write(Command.CD_KICK_2);
    }

    public void openCashDrawerPin5() throws IOException {
        byteArrayOutputStream.write(Command.CD_KICK_5);
    }

    public void close() throws IOException {
        byteArrayOutputStream.close();
    }

}
