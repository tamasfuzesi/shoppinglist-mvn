package cristo.util;

import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.message.MessageFactory;

public class Log extends Logger {
    /**
     * The constructor.
     *
     * @param context        The LoggerContext this Logger is associated with.
     * @param name           The name of the Logger.
     * @param messageFactory The message factory.
     */
    protected Log(LoggerContext context, String name, MessageFactory messageFactory) {
        super(context, name, messageFactory);
    }
}
