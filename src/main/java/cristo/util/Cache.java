package cristo.util;


import java.util.HashMap;
import java.util.Map;

public class Cache<T> {
    private Map<String, T> container = new HashMap<>();
    private int expireInMinutes = 60;
    private int maxSize = 1000;

    public Cache() {
    }

    public void put(String key, T value) {
        container.put(key, value);
    }

    public T get(String key) {
        return container.get(key);
    }

    public void flush() {
        container.clear();
    }

    public void forget(String key) {
        container.remove(key);
    }

    public int getExpireInMinutes() {
        return expireInMinutes;
    }

    public void setExpireInMinutes(int expireInMinutes) {
        this.expireInMinutes = expireInMinutes;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }
}
