package cristo.util;

import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

public class Arr {
    public static ArrayList<Map<String, Object>> resultSetToArrayList(ResultSet resultSet) {
        ArrayList<Map<String, Object>> list = new ArrayList<>();
        try {
            ResultSetMetaData metaData = resultSet.getMetaData();
            int columns = metaData.getColumnCount();

            while (resultSet.next()) {
                ObservableMap<String, Object> row = FXCollections.observableHashMap();
                for (int i = 1; i <= columns; ++i) {
                    row.put(metaData.getColumnName(i), resultSet.getObject(i));
                }
                list.add(row);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}
