package cristo.util;

import shoppinglist.Main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

public class Config {
    private final static Config instance = new Config();

    private String path = "config.properties";

    private Properties properties = new Properties();

    public Config() {
        try {
            properties.load(Objects.requireNonNull(Main.class.getClassLoader().getResourceAsStream(getPath())));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Config Config() {
        return instance;
    }

    public static Config Config(String path) {
        instance.setPath(path);
        return instance;
    }

    public static Object get(String key) {
        return instance.properties.getProperty(key);
    }

    public static void set(String key, String value) {
        instance.properties.setProperty(key, value);
        try {
            File configFile = new File(instance.getFullPath());
            FileWriter writer = new FileWriter(configFile);
            instance.properties.store(writer, "");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean has(String key) {
        return Config.get(key) != null;
    }

    private String getFullPath() {
        return Objects.requireNonNull(Main.class.getClassLoader().getResource(getPath())).getPath();
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
