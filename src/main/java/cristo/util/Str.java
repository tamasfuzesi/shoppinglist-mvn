package cristo.util;

import com.google.common.base.CaseFormat;
import org.atteo.evo.inflector.English;

import java.util.ArrayList;
import java.util.List;

public class Str {
    public static String camelCaseToSnakeCase(String str) {
        str = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, str);

        String[] parts = str.split("_");
        parts[parts.length - 1] = English.plural(parts[parts.length - 1]);

        return String.join("_", parts);
    }

    public static List<String> chunk(String text, int chunkCount) {
        int index = 0;
        List<String> chunkArray = new ArrayList<>();

        while (index < text.length()) {
            chunkArray.add(text.substring(index, Math.min(index + chunkCount, text.length())));
            index += chunkCount;
        }

        return chunkArray;
    }

    public static List<String> justifyToArray(String s, int limit) {
        List<String> chunkArray = new ArrayList<>();
        StringBuilder justifiedLine = new StringBuilder();
        String[] words = s.split(" ");
        for (int i = 0; i < words.length; i++) {
            justifiedLine.append(words[i]).append(" ");
            if (i + 1 == words.length || justifiedLine.length() + words[i + 1].length() > limit) {
                justifiedLine.deleteCharAt(justifiedLine.length() - 1);
                chunkArray.add(justifiedLine.toString());
                justifiedLine = new StringBuilder();
            }
        }
        return chunkArray;
    }

    public static boolean isNumeric(String str) {
        try {
            double number = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static String price(Number price) {
        return price + " Ft";
    }
}
