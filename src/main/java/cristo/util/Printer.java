package cristo.util;

import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import java.util.Arrays;
import java.util.List;

public class Printer {
    private List<PrintService> services;

    private PrintService defaultService;

    private PrintService selectedService;

    public Printer() {
        this.services = getServices();
    }

    public static Printer make() {
        return new Printer();
    }

    public List<PrintService> getServices() {
        return Arrays.asList(PrintServiceLookup.lookupPrintServices(null, null));
    }

    public PrintService findService(String serviceName) {
        if (serviceName == null || serviceName.length() == 0) {
            return null;
        }

        serviceName = serviceName.toLowerCase();

        for (PrintService service : this.services) {
            if (service.getName().toLowerCase().contains(serviceName)) {
                return this.selectedService = service;
            }
        }

        return this.getDefaultService();
    }

    public PrintService getDefaultService() {
        defaultService = PrintServiceLookup.lookupDefaultPrintService();
        return defaultService;
    }

    public PrintService getSelectedService() {
        return selectedService;
    }

    @Override
    public String toString() {
        return "EscBuilder{" +
                "services=" + Console.ANSI_BLUE + services + Console.ANSI_RESET +
                '}';
    }
}
