package cristo.database.builder;

public class WhereCompiler {
    private Where where;

    public WhereCompiler(Where where) {
        this.setWhere(where);
    }

    public static String compile(Where where) {
        WhereCompiler whereCompiler = new WhereCompiler(where);

        return whereCompiler.compile();
    }

    public String compile() {

        return "";
    }

    public Where getWhere() {
        return where;
    }

    public void setWhere(Where where) {
        this.where = where;
    }
}
