package cristo.database.builder;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class NestedWhere implements Whereable {
    private ArrayList<Where> wheres = new ArrayList<>();

    public void add(Where where) {
        this.wheres.add(where);
    }

    @Override
    public String compile() {

        return "()";
    }

    @Override
    public String toString() {
        return compile();
    }

    public ArrayList getQueryData() {
        return (ArrayList) this.wheres.stream()
                .map(Where::getValue)
                .collect(Collectors.toList());
    }
}
