package cristo.database.builder;

public interface Whereable {
    String compile();
}
