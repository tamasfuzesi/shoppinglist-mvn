package cristo.database.builder;

public class Where implements Whereable {
    private Integer index;
    private String column;
    private String operator;
    private String value;
    private String logical;

    public Where(String column, String operator, String value, String logical) {
        this.column = column;
        this.operator = operator;
        this.value = value;
        this.logical = logical;
    }

    @Override
    public String compile() {
        return this.getColumn() + " " + this.getOperator() + " ? ";
    }

    @Override
    public String toString() {
        return this.compile();
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLogical() {
        return logical;
    }

    public void setLogical(String logical) {
        this.logical = logical;
    }
}
