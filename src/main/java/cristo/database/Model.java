package cristo.database;

import cristo.util.Arr;
import cristo.util.Str;
import org.jooq.DSLContext;
import org.jooq.Record;

import java.util.ArrayList;
import java.util.Map;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

public abstract class Model {
    protected String primaryKey = "id";
    protected String table;
    protected Map<String, String> showInTable = Map.of();
    private Builder builder = Builder.make();

    public Model() {
        builder.setTable(this.getTable());
    }

    public ArrayList<Map<String, Object>> all() {
        return Arr.resultSetToArrayList(this.getBuilder()
                .selectFrom(this.getTable())
                .fetchResultSet());
    }

    public Record find(int id) {
        Record data = this.getBuilder().selectFrom(this.getTable())
                .where(this.getPrimaryKey() + " = ?", Integer.toString(id))
                .fetchOne();
        return data;
    }

//    public SelectJoinStep<Record> select(String ...args) {
//        return this.getBuilder()
//                .select(table(""));
//    }

    public String getTable() {
        if (table != null) {
            return table;
        }

        String className = getClass().getSimpleName();
        className = Str.camelCaseToSnakeCase(className);
        this.setTable(className);

        return className;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public DSLContext getBuilder() {
        return builder.getCreate();
    }

    public DSLContext builder() {
        return this.getBuilder();
    }

    public String getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(String primaryKey) {
        this.primaryKey = primaryKey;
    }

    public Map<String, String> getShowInTable() {
        return showInTable;
    }

    public void setShowInTable(Map<String, String> showInTable) {
        this.showInTable = showInTable;
    }

    public Record last() {
        return getBuilder()
                .selectFrom(table(getTable()))
                .orderBy(field("id").desc())
                .limit(1)
                .fetchOne();
    }
}
