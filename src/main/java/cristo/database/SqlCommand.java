package cristo.database;

public enum SqlCommand {
    SELECT,
    INSERT,
    DELETE,
    UPDATE
}
