package cristo.database;

import cristo.util.Config;
import org.jooq.SQLDialect;

import java.nio.file.FileSystems;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connector {
    private Connection connection = null;
    private SQLDialect dialect;

    public Connector() {
        if (connection == null) {
            connect();
        }
    }

    public static Connector getInstance() {
        return new Connector();
    }

    private Connection connect() {
        String url = "jdbc:sqlite:" + this.getDbPath();
        try {
            connection = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return connection;
    }

    private String getDbPath() {
        String path = (String) Config.get("db_file_path");
        return path != null && !path.isEmpty()
                ? path
                : FileSystems.getDefault()
                .getPath("db/database.db")
                .normalize()
                .toAbsolutePath()
                .toString();
    }

    public Connection getConnection() {
        return connection;
    }

    public void setDialect(SQLDialect dialect) {
        this.dialect = dialect;
    }

    public SQLDialect getDialect() {
        return SQLDialect.SQLITE;
    }
}
