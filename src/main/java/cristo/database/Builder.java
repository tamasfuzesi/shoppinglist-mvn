package cristo.database;

import org.jooq.DSLContext;
import org.jooq.conf.Settings;
import org.jooq.impl.DSL;

import java.sql.Connection;

public class Builder {
    private Connection connection;
    private DSLContext create;
    private String table;

    public Builder(Connector connector) {
        this.setConnection(connector.getConnection());
        this.setCreate(DSL.using(this.getConnection(), connector.getDialect(), new Settings().withExecuteLogging(false)));
    }

    public static Builder make() {
        Connector instance = Connector.getInstance();
        return new Builder(instance);
    }

    public static Builder make(String table) {
        Builder instance = make();
        instance.setTable(table);

        return instance;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public DSLContext getCreate() {
        return create;
    }

    public void setCreate(DSLContext create) {
        this.create = create;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

}
