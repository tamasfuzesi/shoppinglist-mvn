package cristo.ui;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import shoppinglist.Main;

import java.io.IOException;

public class DialogLoader {
    public static void load(String dialogResource, String title) {
        load(dialogResource, title, null, null);
    }

    public static void load(String dialogResource, String title, String iconImage) {
        load(dialogResource, title, iconImage, null);
    }

    public static void load(String dialogResource, String title, String iconImage, Controller parentController) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource(dialogResource));
            Parent root1 = fxmlLoader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle(title);
            stage.initStyle(StageStyle.UNIFIED);
            stage.resizableProperty().setValue(Boolean.FALSE);
            ParentController controller = fxmlLoader.getController();
            controller.setParent(parentController);


            if (iconImage != null) {
                stage.getIcons().add(new Image(Main.class.getResource(iconImage).toExternalForm()));
            }
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
