package cristo.ui;

public interface ParentController<T> {
    void setParent(T controller);

    T getParent();
}
