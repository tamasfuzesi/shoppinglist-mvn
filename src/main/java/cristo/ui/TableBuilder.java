package cristo.ui;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.util.ArrayList;
import java.util.List;

public class TableBuilder {
    private TableView table = new TableView();
    private List<TableColumn> columns = new ArrayList<>();

    public TableBuilder() {

    }

    public static TableBuilder TableBuilder() {
        return new TableBuilder();
    }

    public static TableBuilder TableBuilder(String... titles) {
        TableBuilder tableBuilder = new TableBuilder();
        for (String title : titles) {
            tableBuilder.addColumn(title);
        }

        return tableBuilder;
    }

    public TableBuilder addColumn(TableColumn column) {
        this.columns.add(column);
        return this;
    }

    public TableBuilder addColumn(String title) {
        TableColumn column = new TableColumn(title);
        addColumn(column);
        return this;
    }

    public TableBuilder editable(boolean editable) {
        table.setEditable(editable);
        return this;
    }


    private void buildColumns() {
        for (TableColumn tableColumn : columns) {
            table.getColumns().add(tableColumn);
        }
    }

    public TableView getTable() {
        return table;
    }
}
