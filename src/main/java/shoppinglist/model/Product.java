package shoppinglist.model;

import cristo.database.Model;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import org.jooq.Record;

import java.util.List;
import java.util.Map;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

public class Product extends Model implements Cloneable {
    private int id;
    private String name;
    private String ean;
    private int productCategoryId;
    private SimpleDoubleProperty price = new SimpleDoubleProperty(this, "price");
    private SimpleIntegerProperty quantity = new SimpleIntegerProperty(this, "quantity", 1);
    private ProductCategory category;
    private List<Shop> shops;
    private int positionGroupCode;

    public Product(int id, String name, String ean, Double price, int positionGroupCode) {
        this();
        setId(id);
        setName(name);
        setEan(ean);
        setPrice(price);
        setQuantity(1);
        setPositionGroupCode(positionGroupCode);
    }

    public Product() {
        this.showInTable = Map.ofEntries(
                Map.entry("id", "ID"),
                Map.entry("ean", "EAN"),
                Map.entry("name", "Terméknév")
        );
    }

    public Product save() {
        if (getId() == 0) {
            return insert();
        }
        return update();
    }

    private Product update() {
        getBuilder().update(table(getTable()))
                .set(field("name"), getName())
                .set(field("ean"), getEan())
                .set(field("product_category_id"), getProductCategoryId())
                .set(field("position_group_code"), getPositionGroupCode())
                .where("id =", getId())
                .execute();

        return this;
    }

    private Product insert() {
        int result = getBuilder()
                .insertInto(
                        table(getTable()),
                        field("name"),
                        field("ean"),
                        field("product_category_id"),
                        field("position_group_code")
                )
                .values(getName(), getEan(), getProductCategoryId(), getPositionGroupCode())
                .execute();

        Record last = last();
        setId((Integer) last.get("id"));
        setName((String) last.get("name"));
        setEan((String) last.get("ean"));
        return this;
    }

    public List<Product> getAll() {
        return this.getBuilder()
                .selectFrom(this.getTable())
                .fetchInto(Product.class);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

//    public SimpleIntegerProperty idProperty() {
//        return id;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public SimpleStringProperty nameProperty() {
//        return name;
//    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

//    public SimpleStringProperty eanProperty() {
//        return ean;
//    }

    public double getPrice() {
        return price.get();
    }

    public void setPrice(double price) {
        this.price.set(price);
    }

    public SimpleDoubleProperty priceProperty() {
        return price;
    }

    public int getQuantity() {
        return quantity.get();
    }

    public void setQuantity(int quantity) {
        this.quantity.set(quantity);
    }

    public SimpleIntegerProperty quantityProperty() {
        return quantity;
    }

    public int getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(int productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public int getPositionGroupCode() {
        return positionGroupCode;
    }

    public void setPositionGroupCode(int positionGroupCode) {
        this.positionGroupCode = positionGroupCode;
    }

    public ProductCategory getCategory() {
        category = getBuilder().selectFrom(table(ProductCategory.getInstance().getTable()))
                .where("product_id = ?", getId())
                .fetchOne().into(ProductCategory.class);
        return category;
    }

    public List<Shop> getShops() {
        shops = getBuilder().select()
                .from(table(Shop.getInstance().getTable()).as("s"))
                .join(table(ProductShop.getInstance().getTable()).as("ps")).on("s.id = ps.shop_id")
                .where("ps.product_id = ?", getId())
                .fetchInto(Shop.class);
        return shops;
    }
}
