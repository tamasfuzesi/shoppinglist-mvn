package shoppinglist.model;

import cristo.database.Model;

import java.util.List;

public class Shop extends Model {
    private int id;
    private String name;
    private String zip;
    private String city;
    private String address;

    public List<Shop> getAll() {
        return this.getBuilder()
                .selectFrom(this.getTable())
                .fetchInto(Shop.class);
    }

    public static Shop getInstance() {
        return new Shop();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Shop{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", zip='" + zip + '\'' +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
