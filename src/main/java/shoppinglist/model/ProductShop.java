package shoppinglist.model;

import cristo.database.Model;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

public class ProductShop extends Model {
    public static ProductShop getInstance() {
        return new ProductShop();
    }

    @Override
    public String getTable() {
        return "product_shop";
    }

    public void insert(int product_id, int shop_id) {
        getBuilder()
                .insertInto(table(getTable()), field("product_id"), field("shop_id"))
                .values(product_id, shop_id)
                .execute();
    }
}
