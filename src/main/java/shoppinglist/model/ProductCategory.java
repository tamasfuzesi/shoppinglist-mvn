package shoppinglist.model;

import cristo.database.Model;

import java.util.List;

public class ProductCategory extends Model {
    private int id;
    private String name;

    public List<ProductCategory> getAll() {
        return this.getBuilder()
                .selectFrom(this.getTable())
                .fetchInto(ProductCategory.class);
    }

    public static ProductCategory getInstance() {
        return new ProductCategory();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
