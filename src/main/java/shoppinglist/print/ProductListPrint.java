package shoppinglist.print;

import cristo.escpos.ConnectionException;
import cristo.escpos.EscBuilder;
import cristo.escpos.PrintCommand;
import cristo.escpos.Printable;
import cristo.util.Config;
import shoppinglist.model.Product;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ProductListPrint implements Printable {
    private byte[] bytes;

    private EscBuilder escBuilder = new EscBuilder((String) Config.get("printing_charcode"));

    private ProductListBody productListBody;

    public ProductListPrint(ProductListBody body) {
        productListBody = body;
    }

    private void setHeader() {
        try {
            escBuilder.lineBreak();
            escBuilder.setTextAlignCenter();
            escBuilder.setTextSize2H();
            escBuilder.printLn("BEVÁSÁRLÓ LISTA");
            escBuilder.setTextNormal();
            escBuilder.lineBreak(3);
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setBody() {
        try {
            for (Product product : productListBody.getProducts()) {
                escBuilder.printLn(product.getName());
            }
            escBuilder.lineBreak(3);
        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setFooter() {
        try {
            escBuilder.setTextAlignCenter();
            escBuilder.setTextFontB();
            escBuilder.printLn(DateTimeFormatter.ofPattern("yyy.MM.dd HH:mm:ss").format(LocalDateTime.now()));
            escBuilder.setTextTypeBold();
            escBuilder.printLn("#15");
            escBuilder.lineBreak(2);

        } catch (ConnectionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void build() {
        setHeader();
        setBody();
        setFooter();
    }

    public void print() {
        build();
        new PrintCommand(escBuilder.getBytes()).print();
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }
}
