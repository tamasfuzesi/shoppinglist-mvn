package shoppinglist.print;

import shoppinglist.model.Product;

import java.util.List;

public class ProductListBody {
    private List<Product> products;

    private int listId;

    public ProductListBody(List<Product> products) {
        this.products = products;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
