package shoppinglist;

import cristo.escpos.ConnectionException;
import cristo.util.Cache;
import cristo.util.Config;
import cristo.util.Printer;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.print.*;
import java.io.IOException;
import java.sql.SQLException;

public class Main extends Application {

    public static final Logger log = LogManager.getLogger(Main.class.getName());
    public static final Cache<Object> cache = new Cache<>();

    public static void main(String[] args) throws SQLException, PrintException, IOException, ConnectionException {
        System.getProperties().setProperty("org.jooq.no-logo", "true");
        launch(args);
//        EscBuilder escBuilder = new EscBuilder("hun");
//        escBuilder.setTabPosition(25);
//        escBuilder.setTextFontB();
//        escBuilder.printLn("12345678901234567890123678901234567890123");
//        escBuilder.setTextAlignCenter();
//        escBuilder.setTextTypeBold();
//        escBuilder.setTextSize2H();
//        escBuilder.printLn("NYUGTA");
//        escBuilder.setTextNormal();
//        escBuilder.lineBreak(3);
//        escBuilder.setTabPosition(26);
//        escBuilder.print("sertés darálthús".toUpperCase());
//        escBuilder.tab();
//        escBuilder.print(NumberFormat.getNumberInstance().format(1520));
//        escBuilder.lineBreak();
//        escBuilder.setTabPosition(28);
//        escBuilder.print("alma 1kg".toUpperCase());
//        escBuilder.tab();
//        escBuilder.print("650");
//        escBuilder.lineBreak(2);


//        for (String text : Str.justifyToArray("Hello 1234567891011, my name is not importnant right now.", 22)){
//            escBuilder.print(text.toUpperCase());
//            escBuilder.tab();
//            escBuilder.print(NumberFormat.getNumberInstance().format(150000));
//            escBuilder.lineBreak();
//        }
//        printPrinter(escBuilder.getBytes());

//        String appConfigPath = rootPath + "config.properties";

//        Config.set("major", "1");

        System.exit(0);

    }

    public static void printPrinter(byte[] bytes) throws PrintException {
        Printer printer = new Printer();
        PrintService selectedPrinterService = printer.findService((String) Config.get("selected_printer"));
        PrintService printService = selectedPrinterService != null
                ? selectedPrinterService
                : printer.getDefaultService();
        log.info(printService);
        Doc doc = new SimpleDoc(bytes, DocFlavor.BYTE_ARRAY.AUTOSENSE, null);
        DocPrintJob printJob = printService.createPrintJob();
        printJob.print(doc, null);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        BorderPane root = FXMLLoader.load(getClass().getResource("ui/layouts/main.fxml"));
        primaryStage.setScene(new Scene(root));
        primaryStage.setTitle("Shopping List - CRISTO");
        primaryStage.show();
    }
}

