package shoppinglist.ui.content;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import shoppinglist.model.Product;

import java.net.URL;
import java.util.ArrayList;
import java.util.Map;
import java.util.ResourceBundle;

public class ProductTableController implements Initializable {
    @FXML
    private TableView newCartTable;

    private ObservableList data;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Product product = new Product();
        ArrayList<TableColumn<ObservableMap<String, Object>, String>> columns = new ArrayList<>();

        for (Map.Entry<String, String> entry : product.getShowInTable().entrySet()) {
            TableColumn<ObservableMap<String, Object>, String> column = new TableColumn<>(entry.getValue());
            column.setCellValueFactory(cd -> {
                Object value = cd.getValue().get(entry.getKey());
                return new SimpleStringProperty(value != null ? value.toString() : "");
            });
            columns.add(column);
        }

        newCartTable.getColumns().addAll(columns);
        newCartTable.setItems(FXCollections.observableArrayList(product.all()));


    }
}
