package shoppinglist.ui.content.settings;

import cristo.util.Config;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

public class Db implements Initializable {
    @FXML
    private ChoiceBox<String> chbDbType;

    @FXML
    private GridPane paneMysql;

    @FXML
    private HBox paneSqlite;

    @FXML
    private TextField txtHost;

    @FXML
    private TextField txtUser;

    @FXML
    private TextField txtPass;

    @FXML
    private TextField txtDb;

    @FXML
    private Button btnDbPath;

    @FXML
    private TextField txtDbPath;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        chbDbType.getItems().setAll(Arrays.asList("SQLite", "MySQL"));
        chbDbType.setValue((String) Config.get("db_type"));
        chbDbType.setDisable(true);

        paneMysql.setVisible(false);

        txtDbPath.setText(getDbFilePath());

        chbDbType.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                if (chbDbType.getValue().equals("MySQL")) {
                    paneSqlite.setVisible(false);
                    paneMysql.setVisible(true);
                } else {
                    paneSqlite.setVisible(true);
                    paneMysql.setVisible(false);
                }
            }
        });

        btnDbPath.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Stage stage = new Stage();
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Open Resource File");
                fileChooser.setInitialDirectory(
                        new File(System.getProperty("user.home"))
                );
                fileChooser.getExtensionFilters().addAll(
                        new FileChooser.ExtensionFilter("SQLite", "*.db", "*.sqlite")
                );
                File file = fileChooser.showOpenDialog(stage);
                if (file != null) {
                    Config.set("db_file_path", file.getAbsolutePath());
                    txtDbPath.setText(file.getAbsolutePath());
                }
            }
        });
    }

    private String getDbFilePath() {
        Object dbFilePath = Config.get("db_file_path");

        return dbFilePath != null ? dbFilePath.toString() : "";
    }
}
