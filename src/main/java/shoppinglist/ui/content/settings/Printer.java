package shoppinglist.ui.content.settings;

import cristo.util.Config;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.RadioButton;

import javax.print.PrintService;
import java.net.URL;
import java.util.ResourceBundle;

public class Printer implements Initializable {
    @FXML
    ChoiceBox<String> choicePrinter;

    @FXML
    RadioButton radioPrinterType1;

    @FXML
    RadioButton radioPrinterType2;

    @FXML
    private ChoiceBox cbCharcode;

    cristo.util.Printer printer = new cristo.util.Printer();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        for (PrintService printService : printer.getServices()) {
            choicePrinter.getItems().add(printService.getName());
        }

        cbCharcode.getItems().addAll(FXCollections.observableArrayList(Config.get("printing_charcode_list").toString().split(",")));
        cbCharcode.setValue(Config.get("printing_charcode").toString());

        choicePrinter.setValue(getPrinterValue());

        if (Config.has("selected_printer_type")) {
            switch (Config.get("selected_printer_type").toString()) {
                case "58":
                    radioPrinterType1.setSelected(true);
                    break;
                case "80":
                    radioPrinterType2.setSelected(true);
                    break;
            }
        }
    }

    private String getPrinterValue() {
        String defaultPrinter = printer.getDefaultService().getName();
        return defaultPrinter;
    }

    @FXML
    public void save() {
        String printerType = radioPrinterType1.isSelected() ? "58" : "80";

        Config.set("selected_printer", choicePrinter.getValue());
        Config.set("selected_printer_type", printerType);
        Config.set("printing_charcode", cbCharcode.getValue().toString());
    }
}
