package shoppinglist.ui.content;

import cristo.ui.Controller;
import cristo.ui.DialogLoader;
import cristo.util.Str;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.stage.StageStyle;
import javafx.util.Callback;
import shoppinglist.Main;
import shoppinglist.model.Product;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;


public class NewCart implements Initializable, Controller {
    @FXML
    ListView<Product> lvProducts;

    @FXML
    ListView<Product> lvCart;
    Product product = new Product();
    @FXML
    private TextField txtSearch;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        lvProducts.setCellFactory(new Callback<ListView<Product>, ListCell<Product>>() {
            @Override
            public ListCell<Product> call(ListView<Product> param) {
                return new ProductListCell();
            }
        });

        fillProductList();

        lvCart.setCellFactory(new Callback<ListView<Product>, ListCell<Product>>() {
            @Override
            public ListCell<Product> call(ListView<Product> param) {
                return new CartListCell();
            }
        });

        List<Product> products = (List<Product>) Main.cache.get("products");
        if (products != null && !products.isEmpty()) {
            lvCart.setItems(FXCollections.observableArrayList(products));
        }
    }


    public void fillProductList() {
        ObservableList<Product> data = FXCollections.observableArrayList(product.getAll());
        FilteredList<Product> productFilteredList = new FilteredList<>(data, product1 -> true);
        lvProducts.setItems(productFilteredList);

        txtSearch.textProperty().addListener(obs -> {
            String filter = txtSearch.getText();
            if (filter == null || filter.length() == 0) {
                productFilteredList.setPredicate(s -> true);
            } else {
                productFilteredList.setPredicate(s -> s.getName().toLowerCase().contains(filter.toLowerCase()));
            }
        });
    }

    public void addToCart(int index) {
        Main.log.info(index);
    }

    public void addToCart(List indexList) {
        Main.log.info(indexList);
    }

    public void addToCart(Product productItem) {
        if (!inCart(productItem)) {
            lvCart.getItems().add(productItem);

            Main.cache.put("products", lvCart.getItems());
        }
    }

    public void removeFromCart(int index) {
        lvCart.getItems().remove(index);
        Main.cache.put("products", lvCart.getItems());
    }

    private boolean inCart(Product checkProduct) {
        for (Product product : lvCart.getItems()) {
            if (product.getId() == checkProduct.getId()) {
                return true;
            }
        }
        return false;
    }

    public void clearCart(ActionEvent actionEvent) {
        lvCart.getItems().clear();
        Main.cache.forget("products");
    }

    @FXML
    public void openNewProductDialog() {
        DialogLoader.load("ui/layouts/new-product-dialog.fxml", "Új termék hozzáadása", null, this);
    }

    class ProductListCell extends ListCell<Product> {
        HBox hbox = new HBox();
        Label label = new Label("(empty)");
        Label priceLabel = new Label("");
        Pane pane = new Pane();
        Product lastItem;
        Image cartIconImage = new Image(Main.class.getResource("assets/export-80.png").toExternalForm());
        ImageView cartIcon = new ImageView(cartIconImage);

        public ProductListCell() {
            super();
            SplitMenuButton splitMenuButton = new SplitMenuButton();
            splitMenuButton.setText("");
            MenuItem quantityAdd = new MenuItem("Hozzáad");
            quantityAdd.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    TextInputDialog dialog = new TextInputDialog("1");
                    dialog.initStyle(StageStyle.UTILITY);
                    dialog.setTitle("Termék hozzáadása");
                    dialog.setHeaderText("Hány darabot szeretne hozzáadni?");
                    dialog.setContentText("");

                    Optional<String> result = dialog.showAndWait();
                    if (result.isPresent()) {
                        if (Str.isNumeric(result.get())) {
                            getItem().setQuantity(Integer.parseInt(result.get()));
                        }
                    }
                    addToCart(getItem());
                }
            });
            splitMenuButton.getItems().addAll(quantityAdd, new MenuItem("Szerkeszt"), new MenuItem("Töröl"));
            cartIcon.setFitHeight(20);
            cartIcon.setFitWidth(20);
            splitMenuButton.setGraphic(cartIcon);
            label.getStyleClass().add("ListCell");
            priceLabel.getStyleClass().add("ListCell");

            splitMenuButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    addToCart(getItem());
                }
            });
            hbox.getChildren().addAll(label, pane, splitMenuButton);
            HBox.setHgrow(pane, Priority.ALWAYS);
        }

        @Override
        protected void updateItem(Product item, boolean empty) {
            super.updateItem(item, empty);
            setText(null);
            if (empty) {
                lastItem = null;
                setGraphic(null);
            } else {
                lastItem = item;
                label.setText(item != null ? item.getName() : "<null>");
//                priceLabel.setText(item != null ? Str.price(item.getPrice()) : "<null>");
                setGraphic(hbox);
            }
        }
    }

    class CartListCell extends ListCell<Product> {
        HBox hbox = new HBox();
        Label label = new Label("(empty)");
        Label priceLabel = new Label("");
        Label quantityLabel = new Label("");
        Pane pane = new Pane();
        Product lastItem;
        Image trashIconImage = new Image(Main.class.getResource("assets/trash-can-80.png").toExternalForm());
        ImageView trashIcon = new ImageView(trashIconImage);

        public CartListCell() {
            super();
            Button deleteButton = new Button("");
            trashIcon.setFitHeight(20);
            trashIcon.setFitWidth(20);
            deleteButton.setGraphic(trashIcon);
            label.getStyleClass().add("ListCell");
            priceLabel.getStyleClass().add("ListCell");
            quantityLabel.getStyleClass().add("ListCell");
            deleteButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    removeFromCart(getIndex());
                }
            });
            hbox.getChildren().addAll(label, pane, quantityLabel, deleteButton);
            HBox.setHgrow(pane, Priority.ALWAYS);
        }

        @Override
        protected void updateItem(Product item, boolean empty) {
            super.updateItem(item, empty);
            setText(null);
            if (empty) {
                lastItem = null;
                setGraphic(null);
            } else {
                lastItem = item;
                label.setText(item != null ? item.getName() : "<null>");
                priceLabel.setText(item != null ? Str.price(item.getPrice()) : "<null>");
                quantityLabel.setText(item != null ? item.getQuantity() + " db" : "<null>");
                setGraphic(hbox);
            }
        }
    }
}
