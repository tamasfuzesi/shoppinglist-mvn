package shoppinglist.ui.layouts;

import cristo.ui.ParentController;
import cristo.util.Str;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import shoppinglist.model.Product;
import shoppinglist.model.ProductCategory;
import shoppinglist.model.ProductShop;
import shoppinglist.model.Shop;
import shoppinglist.ui.content.NewCart;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

public class NewProductDialog implements Initializable, ParentController<NewCart> {
    private NewCart parentController;

    @FXML
    private TextField txtName;

    @FXML
    private TextField txtEan;

    @FXML
    private TextField txtPrice;

    @FXML
    private ComboBox<ProductCategory> cbCategory;

    @FXML
    private VBox vbShops;

    @FXML
    private TextField txtPositionGroupCode;

    @FXML
    private Button btnSave;

    private HashMap<CheckBox, Shop> cbShopsHashMap = new HashMap<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Shop shop = new Shop();
        ProductCategory productCategory = new ProductCategory();

        shop.getAll().forEach(sh -> {
            CheckBox checkBox = new CheckBox(sh.getName());
            vbShops.getChildren().add(checkBox);
            cbShopsHashMap.put(checkBox, sh);
        });

        cbCategory.setCellFactory(new Callback<ListView<ProductCategory>, ListCell<ProductCategory>>() {
            @Override
            public ListCell<ProductCategory> call(ListView<ProductCategory> param) {
                return new ListCell<ProductCategory>() {
                    @Override
                    protected void updateItem(ProductCategory item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setGraphic(null);
                        } else {
                            setText(item.getName());
                        }
                    }
                };
            }
        });
        cbCategory.setConverter(new StringConverter<ProductCategory>() {
            @Override
            public String toString(ProductCategory item) {
                if (item == null) {
                    return null;
                } else {
                    return item.getName();
                }
            }

            @Override
            public ProductCategory fromString(String itemId) {
                return null;
            }
        });
        cbCategory.getItems().addAll(FXCollections.observableArrayList(productCategory.getAll()));

    }

    @FXML
    public void save() {
        if (!validate()) {
            return;
        }
        Product product = new Product();
        product.setName(txtName.getText());
        product.setEan(txtEan.getText());
        product.setPrice(Double.parseDouble(txtPrice.getText()));
        product.setProductCategoryId(cbCategory.getValue().getId());
        product.setPositionGroupCode(Integer.parseInt(txtPositionGroupCode.getText()));
        product.save();

        saveShops(product.getId());

        getParent().fillProductList();
        Stage stage = (Stage) btnSave.getScene().getWindow();
        stage.close();
    }

    private void saveShops(int productId) {
        ProductShop productShop = new ProductShop();
        for (Shop shop : getSelectedShops()) {
            productShop.insert(productId, shop.getId());
        }
    }

    private void saveProductPrices(int productId, String price) {

    }

    private ArrayList<Shop> getSelectedShops() {
        ArrayList<Shop> shopArrayList = new ArrayList<>();

        for (CheckBox checkbox : cbShopsHashMap.keySet()) {
            if (checkbox.isSelected()) {
                shopArrayList.add(cbShopsHashMap.get(checkbox));
            }
        }

        return shopArrayList;
    }

    private boolean validate() {
        if (txtPrice.getText().isEmpty())
            return false;

        if (txtEan.getText().isEmpty())
            return false;

        return !txtPrice.getText().isEmpty() && Str.isNumeric(txtPrice.getText());
    }


    @Override
    public void setParent(NewCart controller) {
        parentController = controller;
    }

    @Override
    public NewCart getParent() {
        return parentController;
    }
}
