package shoppinglist.controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import shoppinglist.Main;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class SettingsController implements Initializable {
    @FXML
    TreeView<String> settingsTreeView;

    @FXML
    AnchorPane settingsContent;

    private Map<String, String> settingPanes = new HashMap<>() {
        {
            put("Adatbázis", "db");
            put("Nyomtató", "printer");
        }
    };

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        settingsTreeView.setShowRoot(false);

        TreeItem<String> rootItem = new TreeItem<>("Beállítások");

        TreeItem<String> connections = makeGroup("Csatlakozás", rootItem);
        makeGroup("Adatbázis", connections);
        makeGroup("Nyomtató", connections);
        connections.setExpanded(true);

        settingsTreeView.setRoot(rootItem);

        settingsTreeView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                TreeItem<String> selectedItem = (TreeItem<String>) newValue;
                SettingsController.this.changePane(selectedItem.getValue());
            }
        });
    }


    public TreeItem<String> makeGroup(String title, TreeItem<String> parent) {
        TreeItem<String> item = new TreeItem<>(title);
        item.setExpanded(false);
        parent.getChildren().add(item);

        return item;
    }

    public TreeItem<String> makeGroup(String title, TreeItem<String> parent, Node icon) {
        TreeItem<String> item = makeGroup(title, parent);
        item.setGraphic(icon);

        return item;
    }

    @FXML
    public void selectTreeItemEvent(MouseEvent mouseEvent) throws IOException {
        TreeItem<String> item = settingsTreeView.getSelectionModel().getSelectedItem();
        changePane(item.getValue());
    }

    private void changePane(String value) {
        try {
            String pane = settingPanes.get(value);
            if (pane == null) {
                return;
            }

            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("ui/content/settings/" + pane + ".fxml"));
            Parent settingsPane = fxmlLoader.load();
            settingsContent.getChildren().setAll(settingsPane);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
