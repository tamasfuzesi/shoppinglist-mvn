package shoppinglist.controller;

import cristo.ui.Controller;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import shoppinglist.Main;
import shoppinglist.model.Product;
import shoppinglist.print.ProductListBody;
import shoppinglist.print.ProductListPrint;

import java.io.IOException;
import java.util.List;


public class MainController {
    private Controller centerContentController;
    @FXML
    private BorderPane root;

    @FXML
    public void quit() {
        System.exit(0);
    }

    @FXML
    public void openSettingsDialog() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("ui/layouts/settings-dialog.fxml"));
        Parent root1 = fxmlLoader.load();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setTitle("Beállítások");
        stage.initStyle(StageStyle.UNIFIED);
        stage.resizableProperty().setValue(Boolean.FALSE);
        stage.getIcons().add(new Image(Main.class.getResource("assets/settings-80.png").toExternalForm()));
        stage.setScene(new Scene(root1));
        stage.show();
    }

    @FXML
    public void onNewCart() {
        setContent("ui/content/new-cart.fxml");
    }

    @FXML
    public void onProducts() {
        setContent("ui/content/product-table.fxml");
    }

    @FXML
    public void onPrinting() {
        ProductListBody productListBody = new ProductListBody((List<Product>) Main.cache.get("products"));
        new ProductListPrint(productListBody).print();
    }

    private boolean checkProcessingState() {
        return true;
    }

    public void setContent(String path) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource(path));
            if (fxmlLoader.getController() instanceof Controller) {
                setCenterContentController((Controller) fxmlLoader.getController());
            }
            Node content = fxmlLoader.load();
            AnchorPane.setBottomAnchor(content, 0.0);
            AnchorPane.setTopAnchor(content, 0.0);
            AnchorPane.setLeftAnchor(content, 0.0);
            AnchorPane.setRightAnchor(content, 0.0);
            root.setCenter(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Controller getCenterContentController() {
        return centerContentController;
    }

    public void setCenterContentController(Controller centerContentController) {
        this.centerContentController = centerContentController;
    }
}
